import * as React from 'react';
import { shallow } from 'enzyme';
import { AttributeInput } from './AttributeInput';

describe('AttributeInput', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(<AttributeInput />);
    expect(wrapper).toMatchSnapshot();
  });
});
