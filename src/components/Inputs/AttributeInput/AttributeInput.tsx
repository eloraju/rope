import React, { Component } from "react";
import { NumberInput } from "../NumberInput/NumberInput";
import style from "./AttributeInput.module.css";

export type AttributeInputProps = {
  attribute: string;
  points: number;
  modifier: number;
  inputChange: (newVal: number) => void;
};

export const AttributeInput = (props: AttributeInputProps) => {
  return (
    <React.Fragment>
      <div className={style.Attribute}>
        <div className={style.Label}>{props.attribute}</div>

        <div className={style.Input}>
          <NumberInput
            inputValue={props.points}
            changeHandler={props.inputChange}
          />
        </div>

        <div className={style.Output}>{props.modifier}</div>
      </div>
    </React.Fragment>
  );
};
