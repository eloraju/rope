import React from 'react';
import { storiesOf } from '@storybook/react';
import { AttributeInput } from './AttributeInput';

storiesOf('AttributeInput', module).add('AttributeInput', () => (
  <div>
    <AttributeInput attributeId="yolo" value={55}/>
  </div>
));
