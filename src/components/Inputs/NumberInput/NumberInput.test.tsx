import * as React from 'react';
import { shallow } from 'enzyme';
import { NumberInput } from './NumberInput';

describe('NumberInput', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(<NumberInput />);
    expect(wrapper).toMatchSnapshot();
  });
});
