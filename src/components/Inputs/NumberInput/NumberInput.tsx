import React from "react";
import styles from './NumberInput.module.css';

export type NumberInputProps = {
  inputValue: number;
  changeHandler: (newVal: number) => void;
};

export const NumberInput = ({ inputValue, changeHandler }: NumberInputProps) => {
    const parseInput = (input: any) => {
        const asInt = parseInt(input);
        const asStr = input.toString()
        if ( Number.isNaN(asInt)) {
            return 0;
        }

        if (asStr.length > 1 && asStr[0] === "0"){
            return asStr.substring(1)
        }

        return input;
    }

  return (
      <input
      className={styles.NumberInput}
      type="number"
      onChange={(e) => changeHandler(parseInput(e.target.value))}
      value={inputValue}
    />
  );
};
