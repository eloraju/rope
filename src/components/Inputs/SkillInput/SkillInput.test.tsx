import * as React from 'react';
import { shallow } from 'enzyme';
import { SkillInput } from './SkillInput';

describe('SkillInput', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(<SkillInput />);
    expect(wrapper).toMatchSnapshot();
  });
});
