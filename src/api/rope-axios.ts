import axios, { AxiosResponse } from 'axios';


export class Api {
    private baseURL = 'http://localhost:5000/api'

    private axios = axios.create({
        baseURL: this.baseURL
    });

}

