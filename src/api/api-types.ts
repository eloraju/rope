import { GraphQLResolveInfo } from 'graphql';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type RequireFields<T, K extends keyof T> = { [X in Exclude<keyof T, K>]?: T[X] } & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Character = {
  __typename?: 'Character';
  id: Scalars['ID'];
  name: Scalars['String'];
  age?: Maybe<Scalars['Int']>;
  class?: Maybe<Scalars['String']>;
  attributes?: Maybe<CharacterAttributes>;
};

export type CreateCharacterInput = {
  name: Scalars['String'];
  age?: Maybe<Scalars['Int']>;
  class?: Maybe<Scalars['String']>;
};

export type UpdateCharacterInput = {
  name?: Maybe<Scalars['String']>;
  age?: Maybe<Scalars['Int']>;
  class?: Maybe<Scalars['String']>;
};

export type CharacterAttributes = {
  __typename?: 'CharacterAttributes';
  agi?: Maybe<Attribute>;
  str?: Maybe<Attribute>;
  rea?: Maybe<Attribute>;
  per?: Maybe<Attribute>;
};

export type Attribute = {
  __typename?: 'Attribute';
  name?: Maybe<Scalars['String']>;
  shortName?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['Float']>;
  rawValue?: Maybe<Scalars['Float']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  createCharacter: Character;
  createSkill: Skill;
  deleteCharacter: Scalars['Boolean'];
  deleteSkill: Scalars['Boolean'];
  updateCharacter: Scalars['Boolean'];
  updateSkill: Scalars['Boolean'];
};


export type MutationCreateCharacterArgs = {
  args: CreateCharacterInput;
};


export type MutationCreateSkillArgs = {
  args?: Maybe<CreateSkillInput>;
};


export type MutationDeleteCharacterArgs = {
  id: Scalars['String'];
};


export type MutationDeleteSkillArgs = {
  id: Scalars['ID'];
};


export type MutationUpdateCharacterArgs = {
  id: Scalars['ID'];
  args: UpdateCharacterInput;
};


export type MutationUpdateSkillArgs = {
  id: Scalars['ID'];
  args?: Maybe<UpdateSkillInput>;
};

export type Query = {
  __typename?: 'Query';
  character?: Maybe<Character>;
  characters: Array<Maybe<Character>>;
  skill?: Maybe<Skill>;
  skills?: Maybe<Array<Maybe<Skill>>>;
};


export type QueryCharacterArgs = {
  charId: Scalars['ID'];
};


export type QuerySkillArgs = {
  id: Scalars['ID'];
};

export type Skill = {
  __typename?: 'Skill';
  id: Scalars['ID'];
  name: Scalars['String'];
  skillWeigths: Array<Maybe<SkillWeight>>;
};

export type CreateSkillInput = {
  name: Scalars['String'];
  skillWeigths?: Maybe<Array<Maybe<SkillWeightInput>>>;
};

export type UpdateSkillInput = {
  name?: Maybe<Scalars['String']>;
  skillWeigths?: Maybe<Array<Maybe<SkillWeightInput>>>;
};

export type SkillWeightInput = {
  value?: Maybe<Scalars['Float']>;
  rawValue?: Maybe<Scalars['Float']>;
  skill?: Maybe<UpdateSkillInput>;
};

export type SkillWeight = {
  __typename?: 'SkillWeight';
  value?: Maybe<Scalars['Float']>;
  rawValue: Scalars['Float'];
  skill: Skill;
};

export type AttributeWeight = {
  __typename?: 'AttributeWeight';
  name: Scalars['String'];
  value?: Maybe<Scalars['Float']>;
  rawValue: Scalars['Float'];
};



export type ResolverTypeWrapper<T> = Promise<T> | T;


export type LegacyStitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type NewStitchingResolver<TResult, TParent, TContext, TArgs> = {
  selectionSet: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type StitchingResolver<TResult, TParent, TContext, TArgs> = LegacyStitchingResolver<TResult, TParent, TContext, TArgs> | NewStitchingResolver<TResult, TParent, TContext, TArgs>;
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  Character: ResolverTypeWrapper<Character>;
  ID: ResolverTypeWrapper<Scalars['ID']>;
  String: ResolverTypeWrapper<Scalars['String']>;
  Int: ResolverTypeWrapper<Scalars['Int']>;
  CreateCharacterInput: CreateCharacterInput;
  UpdateCharacterInput: UpdateCharacterInput;
  CharacterAttributes: ResolverTypeWrapper<CharacterAttributes>;
  Attribute: ResolverTypeWrapper<Attribute>;
  Float: ResolverTypeWrapper<Scalars['Float']>;
  Mutation: ResolverTypeWrapper<{}>;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
  Query: ResolverTypeWrapper<{}>;
  Skill: ResolverTypeWrapper<Skill>;
  CreateSkillInput: CreateSkillInput;
  UpdateSkillInput: UpdateSkillInput;
  SkillWeightInput: SkillWeightInput;
  SkillWeight: ResolverTypeWrapper<SkillWeight>;
  AttributeWeight: ResolverTypeWrapper<AttributeWeight>;
};

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  Character: Character;
  ID: Scalars['ID'];
  String: Scalars['String'];
  Int: Scalars['Int'];
  CreateCharacterInput: CreateCharacterInput;
  UpdateCharacterInput: UpdateCharacterInput;
  CharacterAttributes: CharacterAttributes;
  Attribute: Attribute;
  Float: Scalars['Float'];
  Mutation: {};
  Boolean: Scalars['Boolean'];
  Query: {};
  Skill: Skill;
  CreateSkillInput: CreateSkillInput;
  UpdateSkillInput: UpdateSkillInput;
  SkillWeightInput: SkillWeightInput;
  SkillWeight: SkillWeight;
  AttributeWeight: AttributeWeight;
};

export type CharacterResolvers<ContextType = any, ParentType extends ResolversParentTypes['Character'] = ResolversParentTypes['Character']> = {
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  age?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  class?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  attributes?: Resolver<Maybe<ResolversTypes['CharacterAttributes']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type CharacterAttributesResolvers<ContextType = any, ParentType extends ResolversParentTypes['CharacterAttributes'] = ResolversParentTypes['CharacterAttributes']> = {
  agi?: Resolver<Maybe<ResolversTypes['Attribute']>, ParentType, ContextType>;
  str?: Resolver<Maybe<ResolversTypes['Attribute']>, ParentType, ContextType>;
  rea?: Resolver<Maybe<ResolversTypes['Attribute']>, ParentType, ContextType>;
  per?: Resolver<Maybe<ResolversTypes['Attribute']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type AttributeResolvers<ContextType = any, ParentType extends ResolversParentTypes['Attribute'] = ResolversParentTypes['Attribute']> = {
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  shortName?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  value?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  rawValue?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type MutationResolvers<ContextType = any, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = {
  createCharacter?: Resolver<ResolversTypes['Character'], ParentType, ContextType, RequireFields<MutationCreateCharacterArgs, 'args'>>;
  createSkill?: Resolver<ResolversTypes['Skill'], ParentType, ContextType, RequireFields<MutationCreateSkillArgs, never>>;
  deleteCharacter?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType, RequireFields<MutationDeleteCharacterArgs, 'id'>>;
  deleteSkill?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType, RequireFields<MutationDeleteSkillArgs, 'id'>>;
  updateCharacter?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType, RequireFields<MutationUpdateCharacterArgs, 'id' | 'args'>>;
  updateSkill?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType, RequireFields<MutationUpdateSkillArgs, 'id'>>;
};

export type QueryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = {
  character?: Resolver<Maybe<ResolversTypes['Character']>, ParentType, ContextType, RequireFields<QueryCharacterArgs, 'charId'>>;
  characters?: Resolver<Array<Maybe<ResolversTypes['Character']>>, ParentType, ContextType>;
  skill?: Resolver<Maybe<ResolversTypes['Skill']>, ParentType, ContextType, RequireFields<QuerySkillArgs, 'id'>>;
  skills?: Resolver<Maybe<Array<Maybe<ResolversTypes['Skill']>>>, ParentType, ContextType>;
};

export type SkillResolvers<ContextType = any, ParentType extends ResolversParentTypes['Skill'] = ResolversParentTypes['Skill']> = {
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  skillWeigths?: Resolver<Array<Maybe<ResolversTypes['SkillWeight']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type SkillWeightResolvers<ContextType = any, ParentType extends ResolversParentTypes['SkillWeight'] = ResolversParentTypes['SkillWeight']> = {
  value?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  rawValue?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  skill?: Resolver<ResolversTypes['Skill'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type AttributeWeightResolvers<ContextType = any, ParentType extends ResolversParentTypes['AttributeWeight'] = ResolversParentTypes['AttributeWeight']> = {
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  value?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  rawValue?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type Resolvers<ContextType = any> = {
  Character?: CharacterResolvers<ContextType>;
  CharacterAttributes?: CharacterAttributesResolvers<ContextType>;
  Attribute?: AttributeResolvers<ContextType>;
  Mutation?: MutationResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
  Skill?: SkillResolvers<ContextType>;
  SkillWeight?: SkillWeightResolvers<ContextType>;
  AttributeWeight?: AttributeWeightResolvers<ContextType>;
};


/**
 * @deprecated
 * Use "Resolvers" root object instead. If you wish to get "IResolvers", add "typesPrefix: I" to your config.
 */
export type IResolvers<ContextType = any> = Resolvers<ContextType>;
