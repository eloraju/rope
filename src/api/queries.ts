import { CreateCharacterInput, CreateSkillInput, Skill, UpdateCharacterInput, UpdateSkillInput } from './api-types';

const Api = {
    all(type: 'characters' | 'skills', fields: string) {
        return `query {
            ${type} {
                ${fields}
            }
        }`
    }
}

export const Characters = {
    all() {
        const fields = `id
        name
        age
        class
        attributes
        `
        return Api.all('characters', fields)
    },

    find(id: string, ): {
    },

    createChracter(args: CreateCharacterInput) {},

    updateCharacter(id: string, args: UpdateCharacterInput){},

    deleteCharacter(id: String!) {},
}

export const Skills = {
    createSkill(args: CreateSkillInput) {},

    updateSkill(id: string, args: UpdateSkillInput){},

    deleteSkill(id: string) {},
}
