import "./App.css";
import React, { Component } from "react";
import { AttributeInput } from "./components/Inputs/AttributeInput/AttributeInput";
import { CharacterCreator } from "./src/containers/CharacterCreator";

export class App extends Component {
  state = {
    val: 10,
  };

    // This is now a placeholder for the character

  changeHandler = (newVal: number) => {
    this.setState({ val: newVal });
  };

    render() {
    return <CharacterCreator characterId="yolo" />
  }
}

export default App;
