export interface Attribute {
    id?: string;
    label: string;
    shortLabel: string;
};

export interface CharacterAttribute {
    attributeId: string;
    val: number;
};