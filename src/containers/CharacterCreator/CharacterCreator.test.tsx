import * as React from 'react';
import { shallow } from 'enzyme';
import { CharacterCreator } from './CharacterCreator';

describe('CharacterCreator', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(<CharacterCreator />);
    expect(wrapper).toMatchSnapshot();
  });
});
