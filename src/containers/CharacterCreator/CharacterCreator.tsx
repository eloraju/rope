import React, { Component } from "react";
import { AttributeInput } from "../../components/Inputs/AttributeInput/AttributeInput";
import styles from "./CharacterCreator.module.css";
//import { Api } from "../../../api/rope-axios";
import { of, Subject } from "rxjs";
import { debounceTime, switchMap } from "rxjs/operators";

export type CharacterCreatorProps = {
  characterId: string;
};

export type CharacterCreatorState = {
  attributeValues: { [id: string]: { points: number; mod: number } };
};

const testCharAttrs = {
  strength: { points: 24, mod: 2 },
  agility: { points: 45, mod: 5 },
  perception: { points: 38, mod: 3 },
  reaction: { points: 12, mod: 1 },
};

export class CharacterCreator extends Component<
  CharacterCreatorProps,
  CharacterCreatorState
> {
    //private api = new Api();
  private inputThrottle = new Subject<{ attr: string; val: number }>();
  private subscription = this.inputThrottle
    .pipe(
      switchMap(({ attr, val }) => {
        const newState = Object.assign({}, this.state);
        newState.attributeValues[attr] = {
          points: val,
          mod: this.state.attributeValues[attr].mod,
        };
        this.setState(newState);
        return of({ attr, val });
      })
    ).pipe(debounceTime(750))
    .subscribe({
      next: ({ attr, val }) => {
        this.inputToOutput(attr, val);
      },
    });

  componentDidMount() {
    this.setState({
      attributeValues: testCharAttrs,
    });
  }

    componentWillUnmount() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

  inputToOutput = (attr: string, val: number) => {
    if (this.state.attributeValues !== null) {
      const args = { [attr]: val };
      console.log(args);
        //      this.api
        //        .calculateAttributeModifier(attr, val)
        //        .then((res) => {
        //          const newState = Object.assign({}, this.state);
        //          newState.attributeValues[attr] = {
        //            points: val,
        //            mod: parseInt(res.data),
        //          };
        //          this.setState(newState);
        //        })
        //        .catch((error) => console.log(error));
    }
  };

  public render() {
    const attrs =
      this.state && this.state.attributeValues
        ? Object.entries(this.state.attributeValues).map(
            ([attr, { points, mod }]) => {
              return (
                <AttributeInput
                  attribute={attr}
                  points={points}
                  modifier={mod}
                  inputChange={(val) => this.inputThrottle.next({ attr, val })}
                />
              );
            }
          )
        : null;

    return <div className={styles.Attributes}>{attrs}</div>;
  }
}
