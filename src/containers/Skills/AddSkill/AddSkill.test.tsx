import * as React from 'react';
import { shallow } from 'enzyme';
import { AddSkill } from './AddSkill';

describe('AddSkill', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(<AddSkill />);
    expect(wrapper).toMatchSnapshot();
  });
});
