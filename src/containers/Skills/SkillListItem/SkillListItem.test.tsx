import * as React from 'react';
import { shallow } from 'enzyme';
import { SkillListItem } from './SkillListItem';

describe('SkillListItem', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(<SkillListItem />);
    expect(wrapper).toMatchSnapshot();
  });
});
