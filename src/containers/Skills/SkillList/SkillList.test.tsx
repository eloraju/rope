import * as React from 'react';
import { shallow } from 'enzyme';
import { SkillList } from './SkillList';

describe('SkillList', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(<SkillList />);
    expect(wrapper).toMatchSnapshot();
  });
});
